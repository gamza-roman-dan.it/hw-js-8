"use strict"
//1-
//це є типу цикл для массивів. іншими словами словами, це метод масиву який очікує у нас колбек функцію
//2-
//метеди які мутують існуючий масив --- arr.push/pop/shift/unshift(), arr.splice(), arr.reverse(), arr.sort()
//метеди які не мутують існуючий масив --- arr.slice(), arr.concat(), arr.forEach/map(), arr.indexOf/lastIndexOf/includes() arr.find/findIndex() arr.filter() arr.split/join() arr.reduce()
//3-
//перевірити можна завдяки Array.isArray() й воно нам поверне true або false
//4-
//map краще використовувати коли ми бажаємо щось повернути з методу або створити новий масив й можливо якийсь видозмінений. а forEach використовуємо коли нам потрібно просто перебрати масив й нічого з нього не повертаючи

//-----------------------------------------------------------------------------------------------------------------------------

//1-

const arrString = ["travel", "hello", "eat", "ski", "lift"];

const arrFilter = arrString.filter(world=>world.length > 3);
const arrResult = arrFilter.length;

console.log(arrResult);

//2-

const somePerson = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Інна", age: 20, sex: "жіноча"},
    {name: "роман", age: 22, sex: "чоловіча"},
    {name: "ліза", age: 27, sex: "жіноча"},
];
const menPerson = somePerson.filter(person => person.sex === "чоловіча");

console.log(menPerson)

//3-
const someArr = ['hello', 'world', 23, '23', null];
const filterBy = (arr, type) => arr.filter(arrType => typeof arrType !== type);

console.log(filterBy(someArr, "string"));
console.log(filterBy(someArr, "number"));
console.log(filterBy(someArr, "object"));


